const PacketType = require('./PacketType');


module.exports = class {
  constructor (type, data, senderId) {
    if (!Object.values(PacketType).includes(type)) {
      throw new Error('PacketType not found.');
    }

    this.type = type;
    this.data = data;
    this.senderId = senderId;

    this.timestamp = Date.now();
  }
};
