module.exports = Object.freeze({
  CONNECTED: 0, // data: { colors[], serverUser }, senderId: server
  USER_CREATED: 1, // data: User, senderId: server
  PLAYER_JOIN: 2, // data: User, senderId: server
  PLAYER_LEAVE: 3, // data: User, senderId: server
  MESSAGE: 4, // data: String, senderId: user
  COLOR_CHANGE: 5, // data: color, senderId: user
  COLOR_ACCEPT: 6, // data: color, senderId: server
  COLOR_REJECT: 7, // data: color, senderId: server
  COLOR_UPDATE: 8, // data: colors[], senderId: server
  POSITION: 9, // data: position, senderId: user
  UPDATE_USER: 10 // data: user, senderId: user
});
