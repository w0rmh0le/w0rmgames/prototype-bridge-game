module.exports = [
  {
    id: 0,
    name: 'server',
    value: '#662030',
    available: false
  },
  {
    id: 1,
    name: 'blue',
    value: '#0062A3',
    available: true
  },
  {
    id: 2,
    name: 'green',
    value: '#006B32',
    available: true
  },
  {
    id: 3,
    name: 'orange',
    value: '#915B03',
    available: true
  },
  {
    id: 4,
    name: 'pink',
    value: '#BD0F5A',
    available: true
  }
];
