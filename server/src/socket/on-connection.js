const { Packet, PacketType } = require('prototype-modules');

const broadcastPacket = require('../utils/broadcast-packet');
const getUUID         = require('../utils/get-uuid');

const colors = require('../colors');
const state  = require('../state');
const User   = require('../User');

const onClose   = require('./on-close');
const onMessage = require('./on-message');


module.exports = function (ws, req) {
  const connectedPacket = new Packet(
    PacketType.CONNECTED,
    { colors: colors, serverUser: state.serverUser },
    state.serverUser.id
  );
  ws.send(JSON.stringify(connectedPacket));
  console.log(`Client at ${req.connection.remoteAddress} connected`);

  const id = getUUID();

  state.sockets.set(id, ws);

  const color = colors.find(x => x.available);
  if (color) {
    color.available = false;
  }
  // TODO: Handle when we run out of colors

  const user = new User('Player', false);
  user.id = id;
  user.active = Boolean(color);
  user.color = color;
  user.player = {
    position: { x: 0, y: 0, z: 0 }
  };

  state.users.set(id, user);

  const userCreatedPacket = new Packet(
    PacketType.USER_CREATED, user, state.serverUser.id
  );
  ws.send(JSON.stringify(userCreatedPacket));

  const playerJoinPacket = new Packet(
    PacketType.PLAYER_JOIN, user, state.serverUser.id
  );
  broadcastPacket(playerJoinPacket, id);

  const colorUpdatePacket = new Packet(
    PacketType.COLOR_UPDATE, state.colors, state.serverUser.id
  );
  broadcastPacket(colorUpdatePacket, state.serverUser.id);

  ws.on('message', onMessage);
  ws.on('close', onClose);
};
