const { Packet, PacketType } = require('prototype-modules');

const broadcastPacket = require('../utils/broadcast-packet');

const state                = require('../state');
const updatePlayerPosition = require('../update-player-position');


module.exports = function (packetString) {
  const p = JSON.parse(packetString);

  switch (p.type) {
  case PacketType.MESSAGE: {
    broadcastPacket(packetString, p.senderId);
    break;
  }

  case PacketType.POSITION: {
    const user = state.users.get(p.senderId);
    updatePlayerPosition(user.player, p.data);
    broadcastPacket(packetString, p.senderId);
    break;
  }

  case PacketType.COLOR_CHANGE: {
    const colorId = p.data.id;
    const color = state.colors.find(x => x.id === colorId);
    const socket = state.sockets.get(p.senderId);
    const user = state.users.get(p.senderId);

    if (user.color.id === colorId) {
      console.log('User tried to change to the same color.');
      break;
    }
    else if (!color.available) {
      const rejectColorPacket = new Packet(
        PacketType.COLOR_REJECT, user.color, state.serverUser.id
      );
      socket.send(JSON.stringify(rejectColorPacket));
      break;
    }

    const oldColor = state.colors.find(x => x.id === user.color.id);
    oldColor.available = true;
    color.available = false;
    user.setColor(color);
    const acceptColorPacket = new Packet(
      PacketType.COLOR_ACCEPT, color, state.serverUser.id
    );
    socket.send(JSON.stringify(acceptColorPacket));

    const colorUpdatePacket = new Packet(
      PacketType.COLOR_UPDATE, state.colors, state.serverUser.id
    );
    broadcastPacket(colorUpdatePacket, state.serverUser.id);
    break;
  }

  case PacketType.UPDATE_USER: {
    state.users.set(p.senderId, p.data);
    broadcastPacket(packetString, p.senderId);
    break;
  }

  default:
    console.error(`Unrecognized PacketType received from ${this._socket._peername.address}`);
    break;
  }
};
