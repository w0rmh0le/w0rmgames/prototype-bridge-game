const { Packet, PacketType } = require('prototype-modules');

const broadcastPacket = require('../utils/broadcast-packet');

const state = require ('../state');


module.exports = function () {
  console.log(`Client at ${this._socket._peername.address} disconnected.`);
  for (const [id, socket] of state.sockets) {
    if (socket.readyState > 1 ) { // 2 === 'CLOSING', 3 === 'CLOSED'
      const user = state.users.get(id);
      const color = state.colors.find(x => x.id === user.color.id);
      color.available = true;

      const colorUpdatePacket = new Packet(
        PacketType.COLOR_UPDATE, state.colors, state.serverUser.id
      );
      broadcastPacket(colorUpdatePacket, id);

      const playerLeavePacket = new Packet(
        PacketType.PLAYER_LEAVE, user, state.serverUser.id
      );
      broadcastPacket(playerLeavePacket, id);

      state.sockets.delete(id);
      state.users.delete(id);
      console.log('Deleted references to closed socket');
    }
  }
};
