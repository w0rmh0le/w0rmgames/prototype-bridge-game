module.exports = function (player, packetData) {
  player.position = {
    x: packetData.x,
    y: packetData.y,
    z: packetData.z
  };
};
