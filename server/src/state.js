const colors = require('./colors');


module.exports = {
  colors,
  serverUser: {
    id: 0,
    username: 'Server',
    color: colors[0]
  },
  sockets: new Map(),
  users: new Map(),
  wss: null
};
