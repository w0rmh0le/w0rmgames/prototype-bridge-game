const state = require ('../state');


module.exports = function (packet, senderId) {
  const p = (typeof packet === 'string') ? packet : JSON.stringify(packet);
  for (const [id, socket] of state.sockets) {
    if (socket.readyState === 1 && id !== senderId) {
      socket.send(p);
    }
  }
};
