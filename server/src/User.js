module.exports = class {
  constructor (username, timestamps) {
    this.username = username;
    this.timestamps = timestamps;
    this.color = null;
    this.active = true;
    this.ready = false;
  }

  setUsername (username) {
    this.username = username;
  }

  setTimestamps (timestamps) {
    this.timestamps = timestamps;
  }

  setColor (color) {
    this.color = color;
  }

  setActive (active) {
    this.active = active;
  }

  setReady (ready) {
    this.ready = ready;
  }
};
