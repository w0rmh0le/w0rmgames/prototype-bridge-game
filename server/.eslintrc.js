module.exports = {
  "env": {
    "es6": true,
    "node": true
  },
  "parserOptions": {
    "sourceType": "module",
    "ecmaVersion": 2017
  },
  "extends": "eslint:recommended",
  "rules": {
    "indent": [
      "error",
      2
    ],
    "linebreak-style": [
        "error",
        "unix"
    ],
    "no-console": "off",
    "quotes": [
        "error",
        "single"
    ],
    "semi": [
        "error",
        "always"
    ]
  }
};
