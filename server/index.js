const WebSocket = require('ws');

const onConnection = require('./src/socket/on-connection');

const state = require('./src/state');


const port = parseInt(process.argv[2]) || 8142;
state.wss = new WebSocket.Server({ port });

state.wss.on('connection', onConnection);

state.wss.on('listening', () => {
  console.log(`WebSocket server listening on port ${port}`);
});

state.wss.on('error', console.error);
