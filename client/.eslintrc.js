module.exports = {
  "env": {
    "es6": true,
    "browser": true,
    "node": true
  },
  "parserOptions": {
    "sourceType": "module",
    "ecmaVersion": 2018
  },
  "extends": "eslint:recommended",
  "rules": {
    "array-bracket-spacing": [
      "error"
    ],
    "block-spacing": [
      "error"
    ],
    "brace-style": [
      "error",
      "stroustrup"
    ],
    "comma-spacing": [
      "error"
    ],
    "curly": [
      "error"
    ],
    "indent": [
      "error",
      2
    ],
    "key-spacing": [
      "error",
      {
        "afterColon": true
      }
    ],
    "max-len": [
      "warn",
      {
        "code": 80,
        "ignoreTrailingComments": true,
        "ignoreUrls": true
      }
    ],
    "multiline-ternary": [
      "error"
    ],
    "no-console": [
      "warn"
    ],
    "no-multiple-empty-lines": [
      "error"
    ],
    "object-curly-spacing": [
      "error",
      "always"
    ],
    "quotes": [
        "error",
        "single"
    ],
    "semi": [
        "error",
        "always"
    ],
    "space-before-blocks": [
      "error"
    ],
    "space-before-function-paren": [
      "error"
    ]
  }
};
