import chat from './chat';
import client from './client';
import loginModal from './login-modal';


// TODO: Replace with env vars later
const host = 'localhost';
const port = 8142;

if (process.env.NODE_ENV !== 'production') {
  console.log('Running in development mode!'); // eslint-disable-line no-console
}

client.init(host, port);
loginModal.init();
chat.init();
