import { PacketType, Packet } from 'prototype-modules';

import client from './client';
import state from './state';


export default {
  chatContainer: undefined,
  chatMessages: undefined,
  chatInput: undefined,
  chatSubmit: undefined,

  getMessageFromPacket (packet) {
    let sender = (client.user.id === packet.senderId)
      ? client.user
      : state.users.get(packet.senderId);
    const color = state.colors.find(x => x.id === sender.color.id);
    let m = `<span style="color: ${color.value}">`;
    if (client.user.timestamps) {
      m += `${new Date(packet.timestamp).toLocaleTimeString()} `;
    }
    m += `${sender.username}</span>: ${packet.data}`;

    return m;
  },

  init () {
    this.chatContainer = document.querySelector('#chat-container');
    this.chatMessages  = document.querySelector('#chat-messages');
    this.chatInput     = document.querySelector('#chat-input');
    this.chatSubmit    = document.querySelector('#chat-submit');

    this.chatSubmit.addEventListener('click', () => {
      this.sendMessage();
    });

    this.chatInput.addEventListener('keydown', (e) => {
      if (e.key === 'Enter') {
        this.sendMessage();
      }
    });
  },

  renderChatMessage (message) {
    const chatMessage = document.createElement('li');
    chatMessage.classList.add('chat-message');
    chatMessage.innerHTML = message;
    this.chatMessages.appendChild(chatMessage);

    handleScroll(chatMessage, this.chatContainer);
  },

  sendMessage () {
    const packet = new Packet(
      PacketType.MESSAGE, this.chatInput.value, client.user.id
    );

    client.ws.send(JSON.stringify(packet));
    const message = this.getMessageFromPacket(packet, client.user.id);
    this.renderChatMessage(message);

    this.chatInput.value = '';
    this.chatInput.focus();
  }
};

function handleScroll (chatMessage, chatContainer) {
  const scrollTrigger = 100;
  const maxScrollTarget
    = chatContainer.scrollHeight
    - (chatContainer.offsetHeight
        + chatContainer.scrollTop
        + chatMessage.clientHeight);

  if (maxScrollTarget < scrollTrigger) {
    chatMessage.scrollIntoView({
      behavior: 'smooth'
    });
  }
}
