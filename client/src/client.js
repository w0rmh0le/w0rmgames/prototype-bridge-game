import { PacketType } from 'prototype-modules';

import chat       from './chat';
import game       from './game';
import loginModal from './login-modal';
import state      from './state';


export default {
  ws: undefined,
  user: undefined,

  onConnected (p) {
    this.updateColors(p.data.colors);
    state.serverUser = p.data.serverUser;
  },

  onUserCreated (p) {
    this.user = p.data;
    loginModal.updateCharacter(this.user.color.name);
  },

  onPlayerJoin (p) {
    state.users.set(p.data.id, p.data);
    if (this.user.ready) {
      game.addPlayer(p.data.id);
    }
  },

  onPlayerLeave (p) {
    if (this.user.ready) {
      game.removePlayer(p.data.id);
    }
    state.users.delete(p.data.id);
  },

  onMessage (p) {
    const m = chat.getMessageFromPacket(p);
    chat.renderChatMessage(m);
  },

  onColorAcceptOrReject (p) {
    this.user.color = p.data;
  },

  onColorUpdate (p) {
    this.updateColors(p.data);
  },

  onPosition (p) {
    const user = state.users.get(p.senderId);
    user.player.position.x = p.data.x;
    user.player.position.y = p.data.y;
    user.player.position.z = p.data.z;
    if (this.user.ready) {
      game.updatePlayerPosition(p.senderId);
    }
  },

  onUpdateUser (p) {
    const oldUser = state.users.get(p.senderId);
    state.users.set(p.senderId, { ...oldUser, ...p.data });
  },

  init (host, port) {
    this.ws = new WebSocket(`ws://${host}:${port}`);

    this.ws.onopen = () => {
      chat.renderChatMessage(`Connecting to ${host}:${port}`);
    };

    this.ws.onmessage = (packet) => {
      const p = JSON.parse(packet.data);

      switch (p.type) {
      case PacketType.CONNECTED:
        this.onConnected(p);
        break;

      case PacketType.USER_CREATED:
        this.onUserCreated(p);
        break;

      case PacketType.PLAYER_JOIN:
        this.onPlayerJoin(p);
        break;

      case PacketType.PLAYER_LEAVE:
        this.onPlayerLeave(p);
        break;

      case PacketType.MESSAGE:
        this.onMessage(p);
        break;

      case PacketType.COLOR_ACCEPT:
        this.onColorAcceptOrReject(p);
        break;

      case PacketType.COLOR_REJECT:
        this.onColorRejectOrReject(p);
        break;

      case PacketType.COLOR_UPDATE:
        this.onColorUpdate(p);
        break;

      case PacketType.POSITION:
        this.onPosition(p);
        break;

      case PacketType.UPDATE_USER:
        this.onUpdateUser(p);
        break;

      default:
        throw new Error(`Unhandled PacketType ${p.type}`);
      }
    };

    this.ws.onclose = () => {
      console.log('WebSocket connection closed.'); // eslint-disable-line no-console
    };

    this.ws.onerror = (err) => {
      console.error(err); // eslint-disable-line no-console
    };
  },

  updateColors (colors) {
    state.colors = colors;
    for (const c of state.colors) {
      if (c.available || (this.user && c.id === this.user.color.id)) {
        loginModal.enableCharacter(c.name);
      }
      else {
        loginModal.disableCharacter(c.name);
      }
    }
  }
};
