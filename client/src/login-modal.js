import { Packet, PacketType } from 'prototype-modules';

import client from './client';
import game   from './game';
import state  from './state';


export default {
  characterBlue: undefined,
  characterGreen: undefined,
  characterOrange: undefined,
  characterPink: undefined,

  modalSubmit: undefined,
  modalTimestamps: undefined,
  modalUsername: undefined,
  modalWrapper: undefined,

  changeTimestamps (e) {
    client.user.timestamps = Boolean(e.target.value);
  },

  changeUsername (e) {
    client.user.username = e.target.value;
  },

  disableCharacter (color) {
    switch (color) {
    case 'blue':
      this.characterBlue.setAttribute('disabled', 'disabled');
      break;

    case 'green':
      this.characterGreen.setAttribute('disabled', 'disabled');
      break;

    case 'orange':
      this.characterOrange.setAttribute('disabled', 'disabled');
      break;

    case 'pink':
      this.characterPink.setAttribute('disabled', 'disabled');
      break;

    case 'server':
      break;

    default:
      console.error('case not found in disableCharacter', color);
      break;
    }
  },

  enableCharacter (color) {
    switch (color) {
    case 'blue':
      this.characterBlue.removeAttribute('disabled');
      break;

    case 'green':
      this.characterGreen.removeAttribute('disabled');
      break;

    case 'orange':
      this.characterOrange.removeAttribute('disabled');
      break;

    case 'pink':
      this.characterPink.removeAttribute('disabled');
      break;

    default:
      console.error('case not found in enableCharacter', color);
      break;
    }
  },

  init () {
    this.characterBlue = document.querySelector('#character-blue');
    this.characterGreen = document.querySelector('#character-green');
    this.characterOrange = document.querySelector('#character-orange');
    this.characterPink = document.querySelector('#character-pink');

    this.modalSubmit = document.querySelector('#modal-submit');
    this.modalTimestamps = document.querySelector('#timestamps');
    this.modalUsername = document.querySelector('#username');
    this.modalWrapper = document.querySelector('#modal-wrapper');

    this.characterBlue.addEventListener('change', this.selectCharacter);
    this.characterGreen.addEventListener('change', this.selectCharacter);
    this.characterOrange.addEventListener('change', this.selectCharacter);
    this.characterPink.addEventListener('change', this.selectCharacter);

    this.modalTimestamps.addEventListener('change', this.changeTimestamps);
    this.modalUsername.addEventListener('change', this.changeUsername);
    this.modalSubmit.addEventListener('click', () => {
      this.submitUser();
    });
  },

  selectCharacter (e) {
    const colorName = e.target.value;
    const color = state.colors.find(x => x.name === colorName);

    if (client.user.color.id !== color.id) {
      client.user.color = color;

      const packet = new Packet(PacketType.COLOR_CHANGE, color, client.user.id);
      client.ws.send(JSON.stringify(packet));
    }
  },

  submitUser () {
    client.user.ready = true;
    const packet = new Packet(
      PacketType.UPDATE_USER, client.user, client.user.id
    );
    client.ws.send(JSON.stringify(packet));

    this.modalWrapper.classList.add('modal-wrapper--closed');
    game.init();
  },

  updateCharacter (color) {
    switch (color) {
    case 'blue':
      this.characterBlue.checked = true;
      break;

    case 'green':
      this.characterGreen.checked = true;
      break;

    case 'orange':
      this.characterOrange.checked = true;
      break;

    case 'pink':
      this.characterPink.checked = true;
      break;

    default:
      console.error('case not found in updateCharacter', color);
      break;
    }
  }
};
