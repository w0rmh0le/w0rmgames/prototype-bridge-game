import Phaser from 'phaser';

import { Packet, PacketType } from 'prototype-modules';

import sky      from './assets/sky.png';
import platform from './assets/platform.png';
import star     from './assets/star.png';
import bomb     from './assets/bomb.png';

import playerBlue   from './assets/wormveloper-blue.png';
import playerGreen  from './assets/wormveloper-green.png';
import playerOrange from './assets/wormveloper-orange.png';
import playerPink   from './assets/wormveloper-pink.png';

import client from './client';
import state  from './state';


class bootGame extends Phaser.Scene {
  constructor () {
    super('BootGame');
  }

  preload () {
    this.load.image('sky', sky);
    this.load.image('ground', platform);
    this.load.image('star', star);
    this.load.image('bomb', bomb);

    this.load.spritesheet(
      'player-blue',
      playerBlue,
      {
        frameWidth: 32,
        frameHeight: 48
      }
    );
    this.load.spritesheet(
      'player-green',
      playerGreen,
      {
        frameWidth: 32,
        frameHeight: 48
      }
    );
    this.load.spritesheet(
      'player-orange',
      playerOrange,
      {
        frameWidth: 32,
        frameHeight: 48
      }
    );
    this.load.spritesheet(
      'player-pink',
      playerPink,
      {
        frameWidth: 32,
        frameHeight: 48
      }
    );
  }

  create () {
    console.log('game is booting...');
    this.scene.start('PlayGame');
  }
}

class playGame extends Phaser.Scene {
  constructor () {
    super('PlayGame');
  }

  create () { // layered in the order they are called
    this.add.image(400, 300, 'sky');

    const platforms = this.physics.add.staticGroup();

    platforms.create(400, 568, 'ground').setScale(2).refreshBody();

    platforms.create(600, 400, 'ground');
    platforms.create(50, 250, 'ground');
    platforms.create(750, 220, 'ground');

    // Animations //////////////////////////////////////////////////////////////

    this.anims.create({
      key: 'left-blue',
      frames: this.anims.generateFrameNumbers(
        'player-blue', { start: 0, end: 3 }
      ),
      frameRate: 10,
      repeat: -1
    });
    this.anims.create({
      key: 'turn-blue',
      frames: [{ key: 'player-blue', frame: 4 }],
      frameRate: 20
    });
    this.anims.create({
      key: 'right-blue',
      frames: this.anims.generateFrameNumbers(
        'player-blue', { start: 5, end: 8 }
      ),
      frameRate: 10,
      repeat: -1
    });

    this.anims.create({
      key: 'left-green',
      frames: this.anims.generateFrameNumbers(
        'player-green', { start: 0, end: 3 }
      ),
      frameRate: 10,
      repeat: -1
    });
    this.anims.create({
      key: 'turn-green',
      frames: [{ key: 'player-green', frame: 4 }],
      frameRate: 20
    });
    this.anims.create({
      key: 'right-green',
      frames: this.anims.generateFrameNumbers(
        'player-green', { start: 5, end: 8 }
      ),
      frameRate: 10,
      repeat: -1
    });

    this.anims.create({
      key: 'left-orange',
      frames: this.anims.generateFrameNumbers(
        'player-orange', { start: 0, end: 3 }
      ),
      frameRate: 10,
      repeat: -1
    });
    this.anims.create({
      key: 'turn-orange',
      frames: [{ key: 'player-orange', frame: 4 }],
      frameRate: 20
    });
    this.anims.create({
      key: 'right-orange',
      frames: this.anims.generateFrameNumbers(
        'player-orange', { start: 5, end: 8 }
      ),
      frameRate: 10,
      repeat: -1
    });

    this.anims.create({
      key: 'left-pink',
      frames: this.anims.generateFrameNumbers(
        'player-pink', { start: 0, end: 3 }
      ),
      frameRate: 10,
      repeat: -1
    });
    this.anims.create({
      key: 'turn-pink',
      frames: [{ key: 'player-pink', frame: 4 }],
      frameRate: 20
    });
    this.anims.create({
      key: 'right-pink',
      frames: this.anims.generateFrameNumbers(
        'player-pink', { start: 5, end: 8 }
      ),
      frameRate: 10,
      repeat: -1
    });

    // Player //////////////////////////////////////////////////////////////////

    this.playerSprite = this.physics.add.sprite(
      100, 450, `player-${client.user.color.name}`
    );
    this.playerSprite.setCollideWorldBounds(true);
    this.playerSprite.body.setGravityY(300);

    client.user.sprite = this.playerSprite;

    this.physics.add.collider(this.playerSprite, platforms);

    this.stars = this.physics.add.group({
      key: 'star',
      repeat: 11,
      setXY: { x: 12, y: 0, stepX: 70 }
    });

    this.stars.children.iterate(function (child) {
      child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
    });

    this.physics.add.collider(this.stars, platforms);

    //check if player overlaps star
    this.physics.add.overlap(this.playerSprite, this.stars, this.collectStar, null, this);

    this.scoreText = this.add.text(
      16,
      16,
      'score: 0',
      {
        fontSize: '32px',
        fill: '#000'
      }
    );

    this.bombs = this.physics.add.group();

    this.physics.add.collider(this.bombs, platforms);
    this.physics.add.collider(this.playerSprite, this.bombs, this.hitBomb, null, this);

    // other players

    for (const entry of state.users) {
      const user = entry[1];
      if (!state.sprites.has(user.id)) {
        const sprite = this.add.sprite(
          user.player.position.x,
          user.player.position.y,
          `player-${user.color.name}`
        );
        state.sprites.set(user.id, sprite);
      }
    }

    this.score = 0;
    this.x = 0;
    this.y = 0;
    this.z = 0;
  }

  update () {
    this.handleInput(this);

    const moved =
      Math.round(this.playerSprite.x) !== Math.round(this.x) ||
      Math.round(this.playerSprite.y) !== Math.round(this.y) ||
      Math.round(this.playerSprite.z) !== Math.round(this.z);

    if (moved) {
      this.x = this.playerSprite.x;
      this.y = this.playerSprite.y;
      this.z = this.playerSprite.z;
      const p = new Packet(
        PacketType.POSITION, { x: this.x, y: this.y, z: this.z }, client.user.id
      );

      client.ws.send(JSON.stringify(p));
    }
  }

  handleInput (gameObject) {
    const cursors = gameObject.input.keyboard.createCursorKeys();

    if (cursors.left.isDown) {
      this.playerSprite.setVelocityX(-160);

      this.playerSprite.anims.play(`left-${client.user.color.name}`, true);
    }
    else if (cursors.right.isDown) {
      this.playerSprite.setVelocityX(160);

      this.playerSprite.anims.play(`right-${client.user.color.name}`, true);
    }
    else {
      this.playerSprite.setVelocityX(0);

      this.playerSprite.anims.play(`turn-${client.user.color.name}`);
    }

    if (cursors.up.isDown && this.playerSprite.body.touching.down) {
      this.playerSprite.setVelocityY(-490);
    }
  }

  collectStar (player, star) {
    if (!this.score) {
      this.score = 0;
    }
    star.disableBody(true, true);

    this.score += 10;
    this.scoreText.setText('Score: ' + this.score);

    if (this.stars.countActive(true) === 0) {
      this.stars.children.iterate(function (child) {
        child.enableBody(true, child.x, 0, true, true);
      });

      const x = (player.x < 400)
        ? Phaser.Math.Between(400, 800)
        : Phaser.Math.Between(0, 400);

      const bomb = this.bombs.create(x, 16, 'bomb');
      bomb.setBounce(1);
      bomb.setCollideWorldBounds(true);
      bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
      bomb.allowGravity = false;
    }
  }

  hitBomb (player) {
    this.physics.pause();
    player.setTint(0xff0000);
    player.anims.play(`turn-${client.user.color.name}`);
    this.gameOver = true;
  }

  addPlayer (userId) {
    const user = state.users.get(userId);

    if (!state.sprites.has(user.id)) {
      const sprite = this.add.sprite(
        user.player.position.x,
        user.player.position.y,
        `player-${user.color.name}`
      );
      state.sprites.set(user.id, sprite);
    }
  }

  removePlayer (userId) {
    if (state.sprites.has(userId)) {
      const sprite = state.sprites.get(userId);
      sprite.destroy();
    }
  }

  updatePlayerPosition (userId) {
    if (!state.users.has(userId) || !state.sprites.has(userId)) {
      return;
    }

    const user = state.users.get(userId);
    const sprite = state.sprites.get(userId);
    if (sprite.x < user.player.position.x) {
      sprite.play(`right-${user.color.name}`, true);
    }
    else if (sprite.x > user.player.position.x) {
      sprite.play(`left-${user.color.name}`, true);
    }
    else {
      sprite.play(`turn-${user.color.name}`);
    }

    sprite.x = user.player.position.x;
    sprite.y = user.player.position.y;
    sprite.z = user.player.position.z;
  }
}

const config = {
  type: Phaser.AUTO,
  width: 800,
  height: 600,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 300 },
      debug: false
    }
  },
  scene: [bootGame, playGame]
};

let game;
let scene;
export default {
  init () {
    game = new Phaser.Game(config);
  },

  addPlayer (userId) {
    setTimeout(() => {
      if (!scene) {
        scene = game.scene.getScene('PlayGame');
      }
      scene.addPlayer(userId);
    }, 250);
  },

  removePlayer (userId) {
    if (!scene) {
      scene = game.scene.getScene('PlayGame');
    }
    if (scene) {
      scene.removePlayer(userId);
    }
  },

  updatePlayerPosition (userId) {
    if (!scene) {
      scene = game.scene.getScene('PlayGame');
    }
    if (scene) {
      scene.updatePlayerPosition(userId);
    }
  }
};
