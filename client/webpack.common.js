const cleanWebpackPlugin = require('clean-webpack-plugin');
const htmlWebpackPlugin  = require('html-webpack-plugin');
const path               = require('path');


module.exports = {
  entry: {
    app: './src/index.js'
  },
  plugins: [
    new cleanWebpackPlugin(['dist']),
    new htmlWebpackPlugin({
      title: 'Bridge Game Client',
      template: 'src/index.html',
      filename: 'index.html'
    }),
  ],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src')
    },
    extensions: ['.js', '.ts']
  },
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|gif|css)$/,
        use: [
          'file-loader'
        ]
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              attrs: ['img:src', 'link:href']
            }
          }
        ]
      }
    ]
  }
};
