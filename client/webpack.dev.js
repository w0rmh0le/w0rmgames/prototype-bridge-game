const Merge              = require('webpack-merge');
const Webpack            = require('webpack');

const Common = require('./webpack.common');


module.exports = Merge(Common, {
  mode: 'development',
  devtool: 'eval-source-map',
  devServer: {
    host: '0.0.0.0',
    compress: true,
    hot: true,
    overlay: true
  },
  plugins: [
    new Webpack.HotModuleReplacementPlugin(),
    new Webpack.NamedModulesPlugin()
  ]
});
